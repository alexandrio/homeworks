# 1. Напишіть гру "rock scissors paper lizard spock". Використайте розділення всієї програми на функції
# (Single-Responsibility principle). Як скелет-заготовку можна використати приклад з заняття.
# 2. До кожної функції напишіть докстрінг або анотацію

from random import choice


figures = ["Rock", "Paper", "Scissors", "Lizard", "Spock"]

"""Умови за яких перемагає користувач у грі"""
winner_rules = {
    'Rock': ['Scissors', 'Lizard'],
    'Paper': ['Rock', 'Spock'],
    'Scissors': ['Paper', 'Lizard'],
    'Lizard': ['Paper', 'Spock'],
    'Spock': ['Rock', 'Scissors']
}


def get_player_figure():
    """
    Функція отримує назву фігури від користувача і перевіряє чи міститься вона у списку [figures] серед допустимих
    :return: Фігуру, яку обрав користувач
    :rtype: str
    """
    while True:
        print(figures)
        player = input("Ваш вибір: ").capitalize()
        if player in figures:
            return player
        else:
            print("\nНевірне  введення, спробуйте ще  раз!\n")


def get_ai_figure():
    """
    Функція обирає випадкову фігуру зі списку [figures]
    :return: Випадкову фігуру, обрану AI
    :rtype: str
    """
    ai = choice(figures)
    print(f"Вибір опонента: {ai}\n")
    return ai


def define_winner(player, ai):
    """
    Функція визначає хто переміг у грі
    :param player: Фігура користувача
    :param ai: Фігура AI
    """
    if player == ai:
        print("Нічия!")
    else:
        for key, value in winner_rules.items():
            if key == player and ai in value:
                print(f"[{player} б'є {ai}] > 'Ви перемогли!'\n")
                break
        else:
            print(f"[{ai} б'є {player}] > 'Ви програли!'\n")



def continue_or_not(go: str) -> bool:
    if go.capitalize() == "X":
        return True
    else:
        return False

def main():
    """
    Головна функція, яка почергово запускає функції в програмі
    """
    while True:
        player = get_player_figure()
        ai = get_ai_figure()
        define_winner(player, ai)
        go = input('Якщо бажаєте вийти натисніть [X + Enter], щоб продовжити натисніть [Enter]\n')
        if continue_or_not(go):
            print("До побачення!")
            quit()
        else:
            continue


main()

