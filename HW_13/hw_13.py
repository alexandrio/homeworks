# Доопрацюйте всі реревірки на типи даних (x, y в Point, begin, end в Line, etc) - зробіть перевірки за допомогою
# property або класса-дескриптора.
# Доопрацюйте класс Triangle з попередньої домашки наступним чином:
# обʼєкти классу Triangle можна порівнювати між собою (==, !=, >, >=, <, <=) за площею.
# перетворення обʼєкту классу Triangle на стрінг показує координати його вершин у форматі x1, y1 -- x2, y2 -- x3, y3


from abc import ABC, abstractmethod
from math import sqrt


class Point:

    def __init__(self, x, y):
        if isinstance(x, (int, float)) and isinstance(y, (int, float)):
            self._x = x
            self._y = y
        else:
            raise TypeError('Значення x та y не відповідають типу int або float')

    def __str__(self):
        return f'{self.x},{self.y}'

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        if isinstance(x, (int, float)):
            self._x = x
        else:
            raise TypeError

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        if isinstance(y, (int, float)):
            self._y = y
        else:
            raise TypeError


class Figure(ABC):

    @abstractmethod
    def __init__(self):
        pass

    def area(self):
        pass

    @abstractmethod
    def length(self):
        pass


class Line(Figure):

    def __init__(self, begin, end):
        if isinstance(begin, Point) and isinstance(end, Point):
            self._begin = begin
            self._end = end
        else:
            raise TypeError('begin, end - не є об\'єктами класу Point')

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, begin):
        if isinstance(begin, Point):
            self._begin = begin
        else:
            raise TypeError

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, end):
        if isinstance(end, Point):
            self._end = end
        else:
            raise TypeError

    def length(self):
        res = ((self.begin.x - self.end.x) ** 2 + (self.begin.y - self.end.y) ** 2) ** 0.5
        return res


class Triangle:
    def __init__(self, point1, point2, point3):
        if isinstance(p1, Point) and isinstance(p2, Point) and isinstance(p3, Point):
            self.point1 = point1
            self.point2 = point2
            self.point3 = point3
            # self.line_a = Line(p1, p2).length()
            # self.line_b = Line(p2, p3).length()
            # self.line_c = Line(p3, p1).length()
        else:
            raise TypeError('point1, point2, point3 - не є об\'єктами класу Point')

    def __str__(self):
        return f'({self.point1} -- {self.point2} -- {self.point3})'

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.area == other.area
        else:
            raise TypeError

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.area != other.area
        else:
            raise TypeError

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.area > other.area
        else:
            raise TypeError

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.area >= other.area
        else:
            raise TypeError

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.area < other.area
        else:
            raise TypeError

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.area <= other.area
        else:
            raise TypeError

    @property
    def point1(self):
        return self._point1

    @point1.setter
    def point1(self, point1):
        if isinstance(point1, Point):
            self._point1 = point1
        else:
            raise TypeError

    @property
    def point2(self):
        return self._point2

    @point2.setter
    def point2(self, point2):
        if isinstance(point2, Point):
            self._point2 = point2
        else:
            raise TypeError

    @property
    def point3(self):
        return self._point3

    @point3.setter
    def point3(self, point3):
        if isinstance(point3, Point):
            self._point3 = point3
        else:
            raise TypeError

    @property
    def line_a(self):
        return Line(self.point1, self.point2)

    @property
    def line_b(self):
        return Line(self.point2, self.point3)

    @property
    def line_c(self):
        return Line(self.point1, self.point3)

    @property
    def total_length(self):
        return self.line_a.length() + self.line_b.length() + self.line_c.length()

    @property
    def semiperimeter(self):
        return self.total_length / 2

    @property
    def area(self):
        return sqrt(self.semiperimeter * (self.semiperimeter - self.line_a.length()) * (
                    self.semiperimeter - self.line_b.length()) * (self.semiperimeter - self.line_c.length()))


p1 = Point(2, 4)
p2 = Point(5, 7)
p3 = Point(10, 6)
l1 = Line(p1, p2)
l2 = Line(p2, p3)
l3 = Line(p3, p1)
triangle_1 = Triangle(p1, p2, p3)

p4 = Point(0, 0)
p5 = Point(5, 1)
p6 = Point(0, 5)
l4 = Line(p4, p5)
l5 = Line(p5, p6)
l6 = Line(p6, p4)
triangle_2 = Triangle(p4, p5, p6)

print(f'Довжина сторони А: {l1.length()}')
print(f'Довжина сторони B: {l2.length()}')
print(f'Довжина сторони C: {l3.length()}')
print(f'Площа трикутника #1: {triangle_1.area}')
print("")
print(f'Довжина сторони А: {l4.length()}')
print(f'Довжина сторони B: {l5.length()}')
print(f'Довжина сторони C: {l6.length()}')
print(f'Площа трикутника #2: {triangle_2.area}')
print("")
print(f'Площа 1 {triangle_1.area} == Площа 2 {triangle_2.area}: {triangle_1 == triangle_2}')
print(f'Площа 1 {triangle_1.area} != Площа 2 {triangle_2.area}: {triangle_1 != triangle_2}')
print(f'Площа 1 {triangle_1.area} > Площа 2 {triangle_2.area}: {triangle_1 > triangle_2}')
print(f'Площа 1 {triangle_1.area} >= Площа 2 {triangle_2.area}: {triangle_1 >= triangle_2}')
print(f'Площа 1 {triangle_1.area} < Площа 2 {triangle_2.area}: {triangle_1 < triangle_2}')
print(f'Площа 1 {triangle_1.area} <= Площа 2 {triangle_2.area}: {triangle_1 <= triangle_2}')
print("")
print(str(triangle_1))
print(str(triangle_2))
