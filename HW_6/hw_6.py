# 1. Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float. Якщо
# перетворити не вдається функція має повернути 0.

print("---Завдання 1---")
print('')

def arg_to_float (arg):
    try:
        return float(arg)
    except Exception as e:
        print(type(e))
        print(e)
        return 0

arg = input("Введіть значення аргументу: ")
print(arg_to_float(arg))
print('')

# 2.Напишіть функцію, що приймає два аргументи. Функція повинна
# a. якщо аргументи відносяться до числових типів (int, float) - повернути перемножене значення цих аргументів,
# b. якщо обидва аргументи це строки (str) - обʼєднати в одну строку та повернути
# c. у будь-якому іншому випадку повернути кортеж з цих аргументів

print("---Завдання 2---")
print('')

def two_args(arg1, arg2):
    if (type(arg1) == int or type(arg1) == float) and (type(arg2) == int or type(arg2) == float):
        return arg1 * arg2
    elif type(arg1) == str and type(arg2) == str:
        return arg1 + arg2
    else:
        return (arg1, arg2)

print("Результат функції: ", two_args(arg2=3, arg1='ffx'))
print("")

# 3. Перепишіть за допомогою функцій вашу программу "Касир в кінотеатрі", яка буде виконувати наступне:
# a. Попросіть користувача ввести свсвій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <> <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <> <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <> <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <> <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <> <>, білетів всеодно нема!"
# b. Замість <> <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік. Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача (1 - рік, 22 - роки, 35 - років і тд...). Наприклад :


def years_ending(age):
    last_digit = age % 10
    if age in range(11, 15) or last_digit in range(5, 10) or last_digit == 0:
        ending = "років"
    elif last_digit in range(2, 5):
        ending = "роки"
    else:
        ending = "рік"
    return ending


def display_text(age, ending):
    if "7" in str(age):
        print(f'Вам {age} {ending}, вам пощастить!')
    elif age < 7:
        print(f'Тобі ж {age} {ending}! Де твої батьки?')
    elif age < 16:
        print(f'Тобі лише {age} {ending}, а це фільм для дорослих!')
    elif age > 65:
        print(f'Вам {age} {ending}? Покажіть пенсійне посвідчення!')
    else:
        print(f'Незважаючи на те, що вам {age} {ending}, білетів всеодно нема!')

def get_and_check_age_input():
    while True:
        try:
            age = int(input('Введіть свій вік: '))
        except:
            print('Невірне введення, введіть свій вік цифрами та спробуйте ще раз!')
        else:
            if age in range(1, 101):  # Якщо віковий ліміт від 1 до 100 років
                return age
            else:
                print('Невірне введення, ви ввели не справжній вік, спробуйте ще раз!')

def main():
    while True:
        age = get_and_check_age_input()
        ending = years_ending(age)
        display_text(age, ending)


print("---Завдання 3---")
print('')
main()
