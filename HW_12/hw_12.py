# 1. Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна було записати тільки обʼєкти класу
# int або float
# 2. Доопрацюйте класс Line так, щоб в атрибути begin та end обʼєктів цього класу можна було записати тільки обʼєкти
# класу Point
# 3. Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point). Реалізуйте перевірку
# даних, аналогічно до класу Line. Визначет метод, що містить площу трикутника. Для обчислень можна використати формулу
# Герона (https://en.wikipedia.org/wiki/Heron%27s_formula)

from abc import ABC, abstractmethod
from math import sqrt


class Point:
    x = None
    y = None

    def __init__(self, x, y):
        if isinstance(x, (int, float)) and isinstance(y, (int, float)):
            self.x = x
            self.y = y
        else:
            raise TypeError('Значення x та y не відповідають типу int або float')


class Figure(ABC):

    @abstractmethod
    def __init__(self):
        pass

    def area(self):
        pass

    @abstractmethod
    def length(self):
        pass


class Line(Figure):
    begin = None
    end = None

    def __init__(self, begin, end):
        if isinstance(begin, Point) and isinstance(end, Point):
            self.begin = begin
            self.end = end
        else:
            raise TypeError('begin, end - не є об\'єктами класу Point')

    def length(self):
        res = ((self.begin.x - self.end.x) ** 2 + (self.begin.y - self.end.y) ** 2) ** 0.5
        return res


class Triangle:
    def __init__(self, point1, point2, point3):
        if isinstance(p1, Point) and isinstance(p2, Point) and isinstance(p3, Point):
            self.point1 = point1
            self.point2 = point2
            self.point3 = point3
            self.line_a = Line(p1, p2).length()
            self.line_b = Line(p2, p3).length()
            self.line_c = Line(p3, p1).length()
        else:
            raise TypeError('point1, point2, point3 - не є об\'єктами класу Point')

    def area(self):
        total_length = self.line_a + self.line_b + self.line_c
        semiperimeter = total_length / 2
        area = sqrt(semiperimeter * (semiperimeter - self.line_a) * (semiperimeter - self.line_b) * (
                    semiperimeter - self.line_c))
        return area


p1 = Point(2, 4)
p2 = Point(5, 7)
p3 = Point(10, 6)
l1 = Line(p1, p2)
l2 = Line(p2, p3)
l3 = Line(p3, p1)
triangle = Triangle(p1, p2, p3)

print(f'Довжина сторони А: {l1.length()}')
print(f'Довжина сторони B: {l2.length()}')
print(f'Довжина сторони C: {l3.length()}')
print("")
print(f'Площа трикутника: {triangle.area()}')
