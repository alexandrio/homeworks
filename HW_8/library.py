from random import choice
from data import *


def get_player_figure():
    """
    Функція отримує назву фігури від користувача і перевіряє чи міститься вона у списку [figures] серед допустимих
    :return: Фігуру, яку обрав користувач
    :rtype: str
    """
    while True:
        print(figures)
        player = input("Ваш вибір: ").capitalize()
        if player in figures:
            return player
        else:
            print("\nНевірне  введення, спробуйте ще  раз!\n")


def get_ai_figure():
    """
    Функція обирає випадкову фігуру зі списку [figures]
    :return: Випадкову фігуру, обрану AI
    :rtype: str
    """
    ai = choice(figures)
    print(f"Вибір опонента: {ai}\n")
    return ai


def define_winner(player, ai):
    """
    Функція визначає хто переміг у грі
    :param player: Фігура користувача
    :param ai: Фігура AI
    """
    if player == ai:
        print("Нічия!")
    else:
        for key, value in winner_rules.items():
            if key == player and ai in value:
                print(f"[{player} б'є {ai}] > 'Ви перемогли!'\n")
                text = (f"Гравець - {player}, АІ - {ai} > Переміг Гравець\n")
                add_to_stat(text)
                break
        else:
            print(f"[{ai} б'є {player}] > 'Ви програли!'\n")
            text = (f"Гравець - {player}, АІ: {ai} > Переміг АІ\n")
            add_to_stat(text)


def add_to_stat(text):
    """
    Функція здійснює запис статистики у файл stat.txt
    :param text: Фігури та результат гри у даному ході
    """
    with open('stat.txt', 'a') as f:
        f.write(text)


def continue_or_not(go: str) -> bool:
    if go.capitalize() == "X":
        return True
    else:
        return False


