"""Умови за яких перемагає користувач у грі"""
winner_rules = {
    'Rock': ['Scissors', 'Lizard'],
    'Paper': ['Rock', 'Spock'],
    'Scissors': ['Paper', 'Lizard'],
    'Lizard': ['Paper', 'Spock'],
    'Spock': ['Rock', 'Scissors']
}

"""Доступні фігури у грі"""
figures = list(winner_rules.keys())
