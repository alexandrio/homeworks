# 1. Напишіть гру "rock scissors paper lizard spock". Використайте розділення всієї програми на функції
# (Single-Responsibility principle). Як скелет-заготовку можна використати приклад з заняття.
# 2. До кожної функції напишіть докстрінг або анотацію

import library


def main():
    """
    Головна функція, яка почергово запускає функції в програмі
    """
    while True:
        player = library.get_player_figure()
        ai = library.get_ai_figure()
        library.define_winner(player, ai)
        go = input('Якщо бажаєте вийти натисніть [X + Enter], щоб продовжити натисніть [Enter]\n')
        if library.continue_or_not(go):
            print("До побачення!")
            quit()
        else:
            continue


main()
