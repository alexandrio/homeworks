# "Касир в кінотеатрі"

age = input('Введіть свій вік: ')
num = int(age)

if age[0] == '0' or not age.isdigit() or len(age) > 2:
    print('Невірне введення, спробуйте ще раз!')
elif len(age) == 2 and age[0] == age[1]:
    print('Як цікаво!')
elif num < 7:
    print('Де твої батьки?')
elif num < 16:
    print('Це фільм для дорослих!')
elif num > 65:
    print('Покажіть пенсійне посвідчення!')
else:
    print('А білетів вже немає!')
