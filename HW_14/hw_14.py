# Візьміть свою гру з HW8 і перепишіть ії в обʼєктному стилі. Зробіть максимум взаємодії (як явної так і неявної) на
# рівні обʼєктів. Рекомендую подумати над такими класами як Player, GameFigure, Game. Памʼятайте про чистоту і простоту
# коду (DRY, KISS), коментарі та докстрінги.

from random import choice


class GameFigures:
    """Доступні фігури та умови за яких перемагає користувач у грі"""
    winner_rules = {
        'Rock': ['Scissors', 'Lizard'],
        'Paper': ['Rock', 'Spock'],
        'Scissors': ['Paper', 'Lizard'],
        'Lizard': ['Paper', 'Spock'],
        'Spock': ['Rock', 'Scissors']
    }
    figures = list(winner_rules.keys())


class Player(GameFigures):
    def get_player_figure(self):
        """
        Функція отримує назву фігури від користувача і перевіряє чи міститься вона у списку [figures] серед допустимих
        :return: Фігуру, яку обрав користувач
        """
        while True:
            print(self.figures)
            player = input("Ваш вибір: ").capitalize()
            if player in self.figures:
                return player
            else:
                print("\nНевірне  введення, спробуйте ще  раз!\n")

    def get_ai_figure(self):
        """
        Функція обирає випадкову фігуру зі списку [figures]
        :return: Випадкову фігуру, обрану AI
        """
        ai = choice(self.figures)
        print(f"Вибір опонента: {ai}\n")
        return ai


class Game(Player):

    def define_winner(self):
        """
        Функція визначає хто переміг у грі
        """
        player = self.get_player_figure()
        ai = self.get_ai_figure()
        if player == ai:
            print("Нічия!")
        else:
            for key, value in self.winner_rules.items():
                if key == player and ai in value:
                    print(f"[{player} б'є {ai}] > 'Ви перемогли!'\n")
                    text = f"Гравець - {player}, АІ - {ai} > Переміг Гравець\n"
                    self.add_to_stat(text)
                    break
            else:
                print(f"[{ai} б'є {player}] > 'Ви програли!'\n")
                text = f"Гравець - {player}, АІ: {ai} > Переміг АІ\n"
                self.add_to_stat(text)

    @staticmethod
    def add_to_stat(text):
        """
        Функція здійснює запис статистики у файл stat.txt
        :param text: Фігури та результат гри у даному ході
        """
        with open('stat.txt', 'a', encoding="utf-8") as f:
            f.write(text)

    def continue_or_not(self):
        """Функція, яка пропонує продовжити гру"""
        go = input('Якщо бажаєте вийти натисніть [X + Enter], щоб продовжити натисніть [Enter]\n')
        if go.capitalize() == "X":
            print("До побачення!")
            quit()
        else:
            self.main()

    def main(self):
        """Функція, яка відповідає за процес гри і перезапуск"""
        self.define_winner()
        self.continue_or_not()


Game().main()
