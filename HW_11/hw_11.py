# Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний
# засіб". Наповніть класи атрибутами на свій розсуд. Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".


class Vehicle:
    type_ = None
    engine_type = None
    manufacturer = None
    fuel_type = None
    max_speed = None
    max_payload = None
    max_range = None


class Car(Vehicle):
    model = None
    car_class = None
    body_style = None
    engine_volume = None
    transmission = None


class Plane(Vehicle):
    ceiling_of_flight = None
    flight_range = None
    crew = None


class Ship(Vehicle):
    tonnage = None
    draft = None
    capacity = None
    crew = None


porsche = Car()
porsche.type_ = 'land'
porsche.engine_type = 'internal combustion engine'
porsche.manufacturer = 'Porsche'
porsche.fuel_type = 'gasoline'
porsche.max_speed = 304
porsche.max_payload = 363
porsche.max_range = 400
porsche.model = '911 Carrera S'
porsche.car_class = 'sport car'
porsche.body_style = '2 door coupe'
porsche.engine_volume = 3.8
porsche.transmission = 'Auto'

tesla = Car()
tesla.type_ = 'land'
tesla.engine_type = 'electric engine'
tesla.manufacturer = 'Tesla'
tesla.fuel_type = 'electricity'
tesla.max_speed = 225
tesla.max_payload = 439
tesla.max_range = 400
tesla.model = 'Model S 70D'
tesla.car_class = 'sport car'
tesla.body_style = '2 door coupe'
tesla.engine_volume = 3.8
tesla.transmission = 'Auto'

f22 = Plane()
f22.type_ = 'air'
f22.engine_type = 'jet engine'
f22.manufacturer = 'Lockheed Martin'
f22.fuel_type = 'jet fuel'
f22.max_speed = 2410
f22.max_payload = 38000
f22.max_range = 3220
f22.ceiling_of_flight = 20000
f22.flight_range = 2960
f22.crew = 1

su27 = Plane()
su27.type_ = 'air'
su27.engine_type = 'jet engine'
su27.manufacturer = 'Sukhoi'
su27.fuel_type = 'jet fuel'
su27.max_speed = 2500
su27.max_payload = 21000
su27.max_range = 2400
su27.ceiling_of_flight = 22500
su27.flight_range = 800
su27.crew = 1

oasis_of_the_seas = Ship()
oasis_of_the_seas.type_ = 'water'
oasis_of_the_seas.engine_type = 'internal combustion engine'
oasis_of_the_seas.manufacturer = 'STX Finland Cruise Oy'
oasis_of_the_seas.fuel_type = 'diesel'
oasis_of_the_seas.max_speed = 41.9
oasis_of_the_seas.max_range = 552
oasis_of_the_seas.tonnage = 225282
oasis_of_the_seas.draft = 9.3
oasis_of_the_seas.capacity = 6880
oasis_of_the_seas.crew = 2200

eclipse_superyacht = Ship()
eclipse_superyacht.type_ = 'water'
eclipse_superyacht.engine_type = 'internal combustion engine'
eclipse_superyacht.manufacturer = 'Blohm+Voss'
eclipse_superyacht.fuel_type = 'diesel'
eclipse_superyacht.max_speed = 40.47
eclipse_superyacht.max_range = 360
eclipse_superyacht.tonnage = 13564
eclipse_superyacht.draft = 5.9
eclipse_superyacht.capacity = 36
eclipse_superyacht.crew = 70
