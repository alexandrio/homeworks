# 1. Напишіть код, який зформує строку, яка містить певну інформацію про символ за його номером у слові. Наприклад "The
# [номер символу] symbol in '[тут слово]' is '[символ з відповідним порядковим номером в слові]'". Слово та номер
# символу отримайте за допомогою input() або скористайтеся константою. Наприклад (слово - "Python" а номер символу 3)
# - "The 3 symbol in 'Python' is 't' ".

while True:
    word = input("Введіть слово: ")
    if not word.isalpha():
        print('Слово повинно містити лише літери!')
        continue
    else:
        break

while True:
    symbol_num = input("Введіть номер символу: ")
    num = int(symbol_num)
    if not symbol_num.isdigit():
        print('Лише цифри!')
        continue
    elif len(word) <= num or num == 0:
        print('Невірне значення! Номер символу більший ніж кількість букв у слові або введено "0"!')
        continue
    else:
        break

result_str = f"The {symbol_num} symbol in '{word}' is '{word[num - 1]}'"
print(result_str)
print("")

# 2. Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою). Напишіть код, який визначить
# кількість слів, в цих даних.

enter_string = input("Введіть строку зі слів: ")
print(f"Кількість слів у строці: {len(enter_string.split())}")
print("")

# 3. Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0,
# 'Lorem Ipsum']. Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int,
# float), які є в lst1. Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []

for val in lst1:
    if type(val) == int or type(val) == float:
        lst2.append(val)
print(lst2)
print("")


