# Напишіть декоратор, який вимірює і виводить на екран час виконання функції в секундах і задекоруйте ним основну
# функцію гри з попередньої дз. Після закінчення гри декоратор має сповістити, скільки тривала гра.


from library import \
    decorator_exec_time,\
    get_player_figure,\
    get_ai_figure,\
    define_winner,\
    continue_or_not


@decorator_exec_time
def main():
    """
    Головна функція, яка почергово запускає функції в програмі
    """
    while True:
        player = get_player_figure()
        ai = get_ai_figure()
        define_winner(player, ai)
        go = input('Якщо бажаєте вийти натисніть [X + Enter], щоб продовжити натисніть [Enter]\n')
        if continue_or_not(go):
            print("До побачення!\n")
            break
        else:
            continue


main()
