# 1. Створіть клас, який реалізує підключення до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ).
# Обʼєкт класу повинен вміти отримувати курс валют станом на певну дату. Обʼєкт класу повинен вміти записати курси в
# текстовий файл. Назва файлу повинна містити дату на яку шукаємо курс, наприклад:
# 21_11_2019.txt
# Дані в файл запишіть у вигляді списку :
# 1. [назва валюти 1] to UAH: [значення курсу до валюти 1]
# 2. [назва валюти 2] to UAH: [значення курсу до валюти 2]
# 3. [назва валюти 3] to UAH: [значення курсу до валюти 3]
# ...
# n. [назва валюти n] to UAH: [значення курсу до валюти n]
# P.S. Архітектура класу - на розсуд розробника. Не забувайте про DRY, KISS, YAGNI, SRP та перевірки!)

import requests
from datetime import datetime


class ExhangeRates:

    def __init__(self, val):
        self.validate_date(val)
        self.date = val

    @classmethod
    def validate_date(cls, val):
        if not datetime(1996, 1, 6).strftime('%Y_%m_%d') <= val.strftime('%Y_%m_%d') <= datetime.now().strftime(
                '%Y_%m_%d'):
            raise Exception("Відсутні дані на цю дату!")

    def connect_to_api(self):
        api_format_date = self.date.strftime('%Y%m%d')
        url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={api_format_date}&json'
        try:
            response = requests.get(url)
            if 300 > response.status_code >= 200 and 'application/json' in response.headers.get('Content-Type', ''):
                try:
                    json_obj = response.json()
                except Exception as e:
                    print(e)
                else:
                    return json_obj
        except Exception as e:
            print(e)

    def write_to_file(self):
        file_format_date = self.date.strftime('%d_%m_%Y')
        with open(f'{file_format_date}.txt', 'wt') as file:
            for item, data in enumerate(self.connect_to_api()):
                rate = data.get('rate', {})
                cc = data.get('cc', {})
                result = f'{item + 1}. {cc} to UAH: {rate} \n'
                file.write(result)
        return print(f"Дані завантажено у файл: {file_format_date}.txt")


input_date = datetime.strptime(input('Введіть дату у наступному форматі - "ДД_ММ_РІК": '), '%d_%m_%Y')
rate_on_date = ExhangeRates(input_date)
rate_on_date.write_to_file()
