from library import multiply_and_raise_to_power


print("")
print('Results:')
print(multiply_and_raise_to_power(2, 2, power=2))
print(multiply_and_raise_to_power(2, 2, power=-1))
print(multiply_and_raise_to_power(2, 1, power=1))
print(multiply_and_raise_to_power(7.0, 1, power=2))
print(multiply_and_raise_to_power(3, 3, power=2.0))
print(multiply_and_raise_to_power(-1, 2.0, power=2))
