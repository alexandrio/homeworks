from library import multiply_and_raise_to_power
import pytest
import math


def test_type_error():
    with pytest.raises(TypeError):
        multiply_and_raise_to_power('-2', 3, power=2)


def test_value_error():
    with pytest.raises(ValueError):
        multiply_and_raise_to_power(math.sqrt(-2), 3, power=2)


@pytest.mark.parametrize('num1, num2, power, expected', [(-2, 3, 2, 36), (4, -2, 2, 64), (3, 3, 2, 81), (3, 1, 3, 27)])
def test_result(num1, num2, power, expected):
    assert multiply_and_raise_to_power(num1, num2, power=power) == expected, 'not expected result'


@pytest.mark.parametrize('num1, num2, power, expected', [(3, 3, 3, int), (5.0, 2.4, 2, float)])
def test_result_type(num1, num2, power, expected):
    assert type(multiply_and_raise_to_power(num1, num2, power=power)) == expected, 'not expected type of data'
