# 1. Дана довільна строка. Напишіть код, який знайде в ній і виведе на екран кількість слів, які містять дві голосні
# літери підряд.

vowels = "АЕЄИІЇОУЮЯаеєиіїоуюя"
text = "Це довільний рядок, що містить слова: сонце, зоопарк, магазин, кооператив, килим, аорта"
print(text)
total_count = 0

for word in text.split():
    vowels_count = 0
    for char in word:
        if char in vowels:
            vowels_count += 1
            if vowels_count == 2:
                total_count += 1
                break
        else:
            vowels_count = 0
print(f'Кількість слів, що містять дві голосні підряд: {total_count}')
print('')

# 2. Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів і цінами: {
# "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166,
# "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}. Напишіть код, який знайде і виведе на екран назви
# магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною. Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"

lower_limit = 35.9
upper_limit = 37.339
match = []
shops_dict = {"cito": 47.999, "BB_studio": 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}
for key, value in shops_dict.items():
    if lower_limit <= float(value) <= upper_limit:
        match.append(f'"{key}"')
print(f'match: {", ".join(match)}')
